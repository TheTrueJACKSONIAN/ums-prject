$(document).ready(function (){

    let form = $('#reg_form');
    form.submit(function (e){
        $(this).attr("disabled","disabled");
        e.preventDefault();

        // disable the submit button
        $("#register_user").prop("disabled", true);
        $.ajax({
            type: form.attr("method"),
            url: form.attr('action'),
            data: form.serialize(),
            //dataType: "json",
            cache: false,
            success: function (data) {

                $("#registration_message").text(data);
                console.log("SUCCESS : ", data);
                $("#register_user").prop("disabled", false);

            },
            error: function (data, e) {

                $("#registration_message").text("an error occurred:");
                console.log(e);
                $("#register_user").prop("disabled", false);
            }
        });
    });
});