<?php
include ('includes/header.php');
//print_r($_SESSION);
//echo "welcome home!";
?>


<div class="container-fluid p-0 top-0 m-0" style="height: 588px; width: 873px;">
    <div class="row">
        <div class="column col-4 top-0 p-4 bg-warning">
            <div class="container-fluid bg-light text-warning pt-3">
                <p>hello!</p>
            </div>
            <div class="container-fluid">

                <?php
                foreach($userUpdateOptions as $option => $item){
                    ?>
                    <ul class="list-group m-1" data-toggle="modal"  data-target="<?php echo '#'.$option.'_modal'; ?>">
                        <li class="list-group-item hover" style="cursor: pointer"> <?php echo $item ?> </li>
                    </ul>
                    <?php
                }
                ?>

                <div class="modal fade" id="userinfo_modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-secondary text-white">
                                <h4 class="modal-title">update your location info</h4>
                                <button type="button" class="btn-close" data-dismiss="modal"></button>
                            </div>
                            <div class="modal-body bg-light">
                                <form id="addr_update_form" enctype="multipart/form-data" action="handlers/update_handler.php" method="post">
                                    <div class="form-group">
                                        <input type="text" name="user_id" id="user_id" value="<?php echo $_SESSION['u_id']; ?>" class="form-control" readonly>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="user_address" id="user_address" class="form-control" placeholder="address eg. no.1 netapps crescent" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="user_city" id="user_city" class="form-control" placeholder="city eg. gwarimpa" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="user_state" id="user_state" class="form-control" placeholder="state eg. abuja" required>
                                    </div>
                                    <div class="d-grid gap-2">
                                        <input type="submit" id="update_addressInfo" name="update_addressInfo" value="update" class="btn btn-success">
                                    </div>
                                    <span id="addr_update_output"></span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal fade" id="userPhoto_modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-secondary text-white">
                                <h4 class="modal-title">upload a profile photo</h4>
                                <button type="button" class="btn-close" data-dismiss="modal"></button>
                            </div>
                            <div class="modal-body bg-light">
                                <form id="photo_update_form" enctype="multipart/form-data" action="handlers/update_handler.php" method="post">
                                    <div class="form-group">
                                        <input type="text" name="user_id" id="user_id" value="<?php echo $_SESSION['u_id']; ?>" class="form-control" readonly>
                                    </div>
                                    <div class="form-group">
                                        <input type="file" name="user_photo" id="user_photo" class="form-control" required>
                                    </div>
                                    <div class="d-grid gap-2">
                                        <input type="submit" id="upload_photo" name="upload_photo" value="upload" class="btn btn-success">
                                    </div>
                                    <span id="photo_update_output"></span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="column col-8 top-0 p-4 bg-white">
            <div class="container-fluid">
                <p class="h5">user profile</p>

            </div>
        </div>
    </div>
</div>
    <script type="text/javascript" src="handlers/reg_handler.js"></script>
<?php
include ('includes/footer.php');
?>