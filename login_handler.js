$(document).ready(function () {
    $('#login_form').on('submit', function () {
        let username = $('#user_name').val().trim();
        let password = $('#password').val().trim();
        if (username !== "" && password !== "") {
            $.ajax({
                url: "handlers/login_handler.php",
                type: "POST",
                data: {
                    user_name: username,
                    password: password
                },
                cache: false,
                success: function (data) {
                    $("#login_message").text(data);
                    console.log("SUCCESS : ", data);
                    $("#login_btn").prop("disabled", false);
                    location.href = "home.php";
                },
                error: function (data, e) {
                    $("#login_message").text("an error occurred:" + data);
                    console.log("ERROR : " + e + " " + data);
                    $("#login_btn").prop("disabled", false);
                }
            });
        } else {
            alert('Please fill all fields!');
        }
        return false;
    });
});