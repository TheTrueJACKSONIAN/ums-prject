<?php
include ('includes/init.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="view-port" content="width=device-width, initial-scale=1.0">
    <title>ums landing page</title>


    <link rel="stylesheet" href="assets/fontawsome/fontawesome.css">
    <link rel="stylesheet" href="assets/fontawsome/fontawesome.min.css">

    <link rel="stylesheet" href="assets/bootstrap-5.0.0-beta1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="assets/bootstrap-5.0.0-beta1-dist/css/bootstrap.min.css">



    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body style="background-image: url('assets/beach.jpg')">
<nav class="navbar navbar-expand-sm bg-light navbar-dark bg-dark">
    <div class="d-flex flex-row-reverse text-white col-12 m-0">
        <h4 class="m-3">cms project</h4>
    </div>
</nav>
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="column">
                <div class="jumbotron bg-transparent mt-5 mb-0" style="height: 176px;">
                    <div class="page-header">
                        <h3>WELCOME TO OUR USER MANAGEMENT SYSTEM</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="d-flex align-items-center justify-content-center container">
                <div class="bg-transparent" style="width: 30%" align="center">
                    <form class="form top-0" id="reg_form" action="handlers/reg_handler.php" method="post">
                        <h3 style="color: rgb(69, 138, 201); font-family: Helvetica Neue; color: white;" >enter details to register...</h3>
                            <div class="form-group">
                                <input type="text" name="user_firstname" id="user_firstname" class="form-control" placeholder="first name" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="user_lastname" id="user_lastname" class="form-control" placeholder="last name" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="user_email" id="user_email" class="form-control" placeholder="email" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="username" id="username" class="form-control" placeholder="username" required>
                            </div>
                            <div class="form-group">
                                <select type="text" name="user_gender" id="user_gender" class="form-control" required>
                                    <option value="" selected>select gender</option>
                                    <option value="male">male</option>
                                    <option value="female">female</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" id="phone" class="form-control" placeholder="phone no. eg. 08012345678" required>
                            </div>
                            <div class="d-grid gap-2">
                                <input type="submit" id="register_user" name="register_user" value="register user" class="btn btn-success">
                            </div>
                        <div>
                            <p id="registration_message"></p>
                            <a id="goto_login"></a>
                        </div>
                    </form>
                </div>
            </div>


    <script type="text/javascript" src="handlers/reg_handler.js"></script>
<?php
include ('includes/footer.php');
?>