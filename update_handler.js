include ()
$(document).ready(function (){
    let form = $('#addr_update_form');
    form.submit(function (e){
        $(this).attr("disabled","disabled");
        e.preventDefault();

        // disable the submit button
        $("#register_user").prop("disabled", true);
        $.ajax({
            type: form.attr("method"),
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "text",
            cache: false,
            success: function (data) {

                $("#addr_update_output").text(data);
                console.log("SUCCESS : ", data);
                $("#update_addressInfo").prop("disabled", false);

            },
            error: function (data, e) {

                $("#addr_update_output").text("an error occurred:");
                console.log(e);
                $("#update_addressInfo").prop("disabled", false);
            }
        });
    });

//photo upload form query
    let photo_form = $('#photo_update_form');
    photo_form.submit(function (e){
        $(this).attr("disabled","disabled");
        e.preventDefault();

        // disable the submit button
        $("#upload_photo").prop("disabled", true);
        $.ajax({
            type: form.attr("method"),
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "text",
            cache: false,
            success: function (data) {

                $("#photo_update_output").text(data);
                console.log("SUCCESS : ", data);
                $("#upload_photo").prop("disabled", false);

            },
            error: function (data, e) {

                $("#photo_update_output").text("an error occurred:");
                console.log(e);
                $("#upload_photo").prop("disabled", false);
            }
        });
    });

});