<?php
include  __DIR__."/config.php";
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, REQUEST');
header('Access-Control-Allow-Headers: X-Requested-With');

session_start();

define('USER_IP', getHostByName(getHostName()));

define('EMAIL_VERIFY_CODE' , substr(md5(mt_rand()),0,15));

if (isset($_GET['logout'])){
    session_destroy();
    header('Refresh:0');
}

$userUpdateOptions =
    [
      'userPhoto'=> "upload profile photo",
      'userinfo'=> "complete profile information"
    ];

