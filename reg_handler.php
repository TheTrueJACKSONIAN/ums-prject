<?php
include __DIR__.'/../includes/init.php';
//include ('../includes/config.php');
header("Content-type:application/json;charset=utf-8");//header is set
$error = false;

if($_SERVER['REQUEST_METHOD'] == 'POST'){

     $user_data = [
        'fname' => $_POST['user_firstname'],
        'lname' => $_POST['user_lastname'],
        'username' => $_POST['username'],
        'gender' => $_POST['user_gender'],
        'phone' => $_POST['phone'],
        'email' => $_POST['user_email'],
        'password' => password_hash('password123', PASSWORD_DEFAULT),
        'created_at' => date("Y-m-d"),
        'user_IP' => USER_IP,
        'verify_code' => EMAIL_VERIFY_CODE
    ];

    try{

        $query = "INSERT INTO user_info (first_name, last_name, user_name, gender, phone, email, password, created, registered_ip_id, email_verify)
                    VALUES (:fname, :lname, :username, :gender, :phone, :email, :password, :created_at, :user_IP, :verify_code)";

        $statement = $conn->prepare($query);

        foreach ($user_data as $data => $data_item){
            $statement->bindParam(':'.$data,  $data_item, PDO::PARAM_STR);
        }
        $statement->execute($user_data);
        $u_id = $conn->lastInsertId();
        $code = $user_data['verify_code'];

        $message = "Your Activation Code is ".EMAIL_VERIFY_CODE."";
        $to = $user_data['email'];
        $subject = "UMS Account Activation";
        $from = 'jaxonianville1590@gmail.com';
        $body='Here is your verification code: \n '.$user_data['verify_code'] .' click the link provided here <a href="127.0.0.1/verification.php">Verification.php?id='.$u_id.'&code='.$user_data['verify_code'].'</a>to activate your account.';
        $headers = "From:".$from;
        mail($to,$subject,$body,$headers);

        echo "Registration, Successful!"."\r\n"."An Activation Code Is Sent To You Check You Emails";

        header('Location:'.'http://127.0.0.1/ums_proj/handlers/verification.php?id='.$u_id.'&code='.$code);//for development purposes
    }catch (PDOException $ex){
        die("ERROR: Registration Incomplete: ".$query."\n".$ex->getMessage());
    }
    unset($statement);
    unset($conn);

}