<?php
include ('includes/init.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="view-port" content="width=device-width, initial-scale=1.0">
    <title>ums landing page</title>


    <link rel="stylesheet" href="assets/fontawsome/fontawesome.css">
    <link rel="stylesheet" href="assets/fontawsome/fontawesome.min.css">

    <link rel="stylesheet" href="assets/bootstrap-5.0.0-beta1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="assets/bootstrap-5.0.0-beta1-dist/css/bootstrap.min.css">



    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body style="background-image: url('assets/beach.jpg')">
<nav class="navbar navbar-expand-sm bg-light navbar-dark bg-dark">
    <div class="d-flex flex-row-reverse text-white col-12 m-0">
        <h4 class="m-3">cms project</h4>
    </div>
</nav>
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="column">
                <div class="jumbotron bg-transparent mt-5">
                    <div class="page-header">
                        <h3>WELCOME TO OUR USER MANAGEMENT SYSTEM</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="d-flex align-items-center justify-content-center container">
                <div class="bg-transparent" style="width: 30%" align="center">
                    <form class="form" id="login_form" action="handlers/login_handler.php" method="post">
                        <h3 style="color: rgb(69, 138, 201); font-family: Helvetica Neue; color: white;" >enter user details to login...</h3>
                    <div class="form-group">
                        <input type="text" id="user_name" name="user_name" class="form-control border-white border-bottom bg-transparent border-3" placeholder="username..." required>
                    </div>
                    <div class="form-group">
                        <input type="password" id="password" name="password" class="form-control border-white border-bottom bg-transparent border-3" placeholder="password..." required>
                    </div>
                    <div class="d-grid gap-2">
                        <input type="submit" id="login_btn" name="login_btn" value="login" class="btn btn-success text-light text">
                    </div>
                    <div>
                        <p id="login_message"></p>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
include ('includes/footer.php');
?>