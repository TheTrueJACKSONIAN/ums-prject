<?php
include ('includes/init.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="view-port" content="width=device-width, initial-scale=1.0">
    <title>ums landing page</title>


    <link rel="stylesheet" href="assets/fontawsome/fontawesome.css">
    <link rel="stylesheet" href="assets/fontawsome/fontawesome.min.css">

    <link rel="stylesheet" href="assets/bootstrap-5.0.0-beta1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="assets/bootstrap-5.0.0-beta1-dist/css/bootstrap.min.css">



    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body style="background-image: url('assets/beach.jpg')">
<nav class="navbar navbar-expand-sm bg-light navbar-dark bg-dark">
    <div class="d-flex flex-row-reverse text-white col-12 m-0">
        <h4 class="m-3">cms project</h4>
    </div>
</nav>
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="column">
                <div class="jumbotron bg-transparent mt-5">
                    <div class="page-header">
                        <h3>WELCOME TO OUR USER MANAGEMENT SYSTEM</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" align="center">
            <div class="container-fluid">
                <div class="btn-group-lg" role="group">
                    <button type="button" onclick="location.href='login_page.php';" class="btn btn-outline-warning border-2 text-white" style="font-size: 40px; font-family: cursive"><span class="glyphicon glyphicon-log-in"></span><b> login</b></button>
                    <button type="button" class="btn btn-secondary text-white"  style="font-size: 40px">register</button>
                </div>
            </div>
        </div>
        <div class="row"></div>
    </div>
</div>



<div class="footer">
    <script src="assets/jquery-3.5.1.min.js"></script>
    <script src="assets/bootstrap-5.0.0-beta1-dist/js/bootstrap.min.js"></script>
    <script src="assets/bootstrap-5.0.0-beta1-dist/js/bootstrap.js"></script>
</div>
</body>
</html>