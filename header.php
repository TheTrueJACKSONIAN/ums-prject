<?php
include ('includes/init.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="view-port" content="width=device-width, initial-scale=1.0">
    <title>ums landing page</title>


    <link rel="stylesheet" href="assets/fontawsome/fontawesome.css">
    <link rel="stylesheet" href="assets/fontawsome/fontawesome.min.css">

    <link rel="stylesheet" href="assets/bootstrap-5.0.0-beta1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="assets/bootstrap-5.0.0-beta1-dist/css/bootstrap.min.css">



    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body style="padding: 0">
<nav class="navbar navbar-expand-sm bg-light navbar-dark bg-dark">
    <div class="d-flex flex-row-reverse text-white col-10 m-0 p-3">
        <h4>cms project</h4>
    </div>
    <div class="container-fluid col-2 m-0 p-3">
            <a href="login_page.php" class="btn btn-outline-warning"><span class="glyphicon glyphicon-log-out"></span>  Logout</a>
        <span class="text-danger" id="logout_message"></span>
    </div>
</nav>