<?php

define('SERVER', 'localhost');
define('DATABASE','ums_proj');
define('USERNAME', 'root');
define('PASSWORD', '');


try {
    $conn = new PDO('mysql:host=' . SERVER .';dbname=' . DATABASE . ';charset=utf8', USERNAME, PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    die("Connection failed!: " . $e->getMessage());
}